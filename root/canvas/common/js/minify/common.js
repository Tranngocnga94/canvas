"use strict";
var _createClass = function() {
    function s(t, i) {
        for (var e = 0; e < i.length; e++) {
            var s = i[e];
            s.enumerable = s.enumerable || !1,
            s.configurable = !0,
            "value"in s && (s.writable = !0),
            Object.defineProperty(t, s.key, s)
        }
    }
    return function(t, i, e) {
        return i && s(t.prototype, i),
        e && s(t, e),
        t
    }
}();
(window, document);
var LetterEffecter = function() {
    function a(t, i, e, s, n) {
    }
}()
, getPointDistance = function(t, i) {
    var e = t
    , s = i
    , n = e.x - s.x
    , a = e.y - s.y;
    return Math.sqrt(Math.pow(n, 2) + Math.pow(a, 2))
},
Circle_02 = function() {
    function i(t) {
        this.init(t)
    }
    return _createClass(i, [{
        key: "init",
        value: function(t) {
            this.canvas = document.getElementById(t.id),
            this.ctx = this.canvas.getContext("2d"),
            this.width = this.canvas.width,
            this.height = this.canvas.height,
            this.radius = this.width / 2,
            this.color = "#fff",
            this.strokeWeight = .2,
            this.points = [],
            this.Pi = Math.PI,
            this.TwoPi = 6.28318,
            this.pointLen = t.pointLen || 30,
            this.count = .03,
            this.isShow = !1,
            this.ctx.fillStyle = this.color,
            this.ctx.strokeStyle = this.color,
            this.ctx.lineWidth = this.strokeWeight;
            for (var i = this.radius - 10, e = 0; e < 4; e++) {
                var s = this.pointLen - this.pointLen / 4 * e | 0
                , n = i - i / 4 * e | 0
                , a = this.TwoPi / 4 * e;
                this.addPoints(s, n, a)
            }
        }
    }, {
        key: "addPoints",
        value: function(t, i, e) {
            for (var s = this.TwoPi / t, n = this.radius, a = [], o = 0; o < t; o++) {
                var h = o * s
                , r = n + Math.cos(h) * i
                , c = n + Math.sin(h) * i;
                a.push({
                    x: 0 | r,
                    y: 0 | c,
                    angle: e
                })
            }
            this.points.push(a)
        }
    }, {
        key: "drawCircle",
        value: function() {
            var t = this.ctx
            , i = this.points
            , e = i.length;
            t.clearRect(0, 0, this.width, this.height),
            t.beginPath();
            for (var s = 0; s < e; s++)
                for (var n = i[s], a = 6 * (((180 * Math.cos(n[0].angle) + 180) / 360 * 100 | 0) / 100) + .5, o = 0; o < n.length; o++) {
                    var h = n[o];
                    h.angle += this.count,
                    t.arc(h.x, h.y, a, 0, this.TwoPi, !1),
                    t.closePath()
                }
                t.fill()
            }
        }, {
            key: "draw",
            value: function() {
                if (!this.isShow)
                    return !1;
                this.drawCircle()
            }
        }]),
    i
}() //number 2
, 
Circle_01 = function() {
    function i(t) {
        this.init(t)
    }
    return _createClass(i, [{
        key: "init",
        value: function(t) {
            this.canvas = document.getElementById(t.id),
            this.ctx = this.canvas.getContext("2d"),
            this.width = this.canvas.width,
            this.height = this.canvas.height,
            this.radius = this.width / 2,
            this.color = "#f90",
            this.strokeWeight = 1,
            this.points = [],
            this.Pi = Math.PI,
            this.TwoPi = 6.28318,
            this.pointLen = t.pointLen || 30,
            this.count = -.002,
            this.isShow = !1,
            this.ctx.strokeStyle = this.color,
            this.ctx.lineWidth = this.strokeWeight,
            this.ctx.fillStyle = this.color,
            this.addPoints(this.pointLen)
        }
    }, {
        key: "addPoints",
        value: function(t) {
            for (var i = this.TwoPi / 2 / t, e = this.radius, s = 0; s < t; s++) {
                var n = s * i
                , a = e + Math.cos(n) * e
                , o = e + Math.sin(n) * e
                , h = e + Math.cos(-n) * e
                , r = e + Math.sin(-n) * e;
                this.points.push({
                    x: a,
                    y: o,
                    x2: h,
                    y2: r,
                    angle: n
                })
            }
        }
    }, {
        key: "drawCircle",
        value: function() {
            var t = this.ctx
            , i = this.points
            , e = i.length
            , s = this.radius;
            t.clearRect(0, 0, this.width, this.height);
            for (var n = 0; n < e; n++) {
                var a = i[n]
                , o = a.angle;
                a.x = s + Math.cos(o) * s,
                a.y = s + Math.sin(o) * s,
                a.x2 = s + Math.cos(-o) * s,
                a.y2 = s + Math.sin(-o) * s,
                a.angle += this.count,
                a.angle < 0 && (a.angle = this.Pi);
                var h = getPointDistance({
                    x: a.x,
                    y: a.y
                }, {
                    x: a.x2,
                    y: a.y2
                });
                t.fillRect(a.x2, a.y2, 1, h)
            }
        }
    }, {
        key: "draw",
        value: function() {
            if (!this.isShow)
                return !1;
            this.drawCircle()
        }
    }]),
    i
}() //number 1
, Circle_03 = function() {
    function i(t) {
        this.init(t)
    }
    return _createClass(i, [{
        key: "init",
        value: function(t) {
            this.canvas = document.getElementById(t.id),
            this.ctx = this.canvas.getContext("2d"),
            this.width = this.canvas.width,
            this.height = this.canvas.height,
            this.radius = this.width / 2,
            this.color = "#f00",
            this.strokeWeight = 1,
            this.points = [],
            this.Pi = Math.PI,
            this.TwoPi = 6.28318,
            this.pointLen = t.pointLen || 20,
            this.count = -.01,
            this.isShow = !1,
            this.ctx.fillStyle = "rgba(255,255,255,0.1)",
            this.ctx.strokeStyle = this.color,
            this.ctx.lineWidth = this.strokeWeight;
            for (var i = this.radius - 35, e = 0; e < 3; e++) {
                var s = this.pointLen - this.pointLen / 3 * e | 0
                , n = i - i / 3 * e | 0;
                this.addPoints(s, n)
            }
        }
    }, {
        key: "addPoints",
        value: function(t, i) {
            for (var e = this.TwoPi / t, s = this.radius, n = [], a = 0; a < t; a++) {
                var o = a * e
                , h = s + Math.cos(o) * i
                , r = s + Math.sin(o) * i;
                n.push({
                    x: 0 | h,
                    y: 0 | r,
                    angle: o
                })
            }
            this.points.push(n)
        }
    }, {
        key: "drawCircle",
        value: function() {
            var t = this.ctx
            , i = this.points
            , e = i.length;
            t.clearRect(0, 0, this.width, this.height);
            for (var s = 0; s < e; s++)
                for (var n = i[s], a = 0; a < n.length; a++) {
                    var o = n[a]
                    , h = 30 * (((180 * Math.cos(o.angle) + 180) / 360 * 100 | 0) / 100) + 5;
                    o.angle += this.count,
                    t.beginPath(),
                    t.arc(o.x, o.y, h, 0, this.TwoPi, !1),
                    t.stroke()
                }
            }
        }, {
            key: "draw",
            value: function() {
                if (!this.isShow)
                    return !1;
                this.drawCircle()
            }
        }]),
    i
}() // number 3
, Circle_04 = function() {
    function i(t) {
        this.init(t)
    }
    return _createClass(i, [{
        key: "init",
        value: function(t) {
            this.canvas = document.getElementById(t.id),
            this.ctx = this.canvas.getContext("2d"),
            this.width = this.canvas.width,
            this.height = this.canvas.height,
            this.radius = this.width / 2,
            this.color = "#00f",
            this.strokeWeight = 1,
            this.points = [],
            this.Pi = Math.PI,
            this.TwoPi = 6.28318,
            this.pointLen = t.pointLen || 50,
            this.count = -.002,
            this.isShow = !1,
            this.ctx.strokeStyle = this.color,
            this.ctx.lineWidth = this.strokeWeight,
            this.addPoints(this.pointLen)
        }
    }, {
        key: "addPoints",
        value: function(t) {
            for (var i = this.TwoPi / t, e = this.radius, s = 0; s < t; s++) {
                var n = s * i
                , a = e + Math.cos(n) * e
                , o = e + Math.sin(n) * e
                , h = e + Math.cos(-n) * e
                , r = e + Math.sin(-n) * e;
                this.points.push({
                    x: a,
                    y: o,
                    x2: h,
                    y2: r,
                    angle: n
                })
            }
        }
    }, {
        key: "drawCircle",
        value: function() {
            var t = this.ctx
            , i = this.points
            , e = i.length
            , s = this.radius;
            t.clearRect(0, 0, this.width, this.height),
            t.beginPath();
            for (var n = 0; n < e; n++) {
                var a = i[n]
                , o = a.angle;
                a.x = s + Math.cos(o) * s,
                a.x2 = -a.x,
                a.y = s + Math.sin(o) * s,
                a.y2 = s + Math.sin(-o) * s,
                a.angle += this.count,
                t.moveTo(0 | a.x, a.y),
                t.lineTo(a.x2, 0 | a.y2)
            }
            t.stroke()
        }
    }, {
        key: "draw",
        value: function() {
            if (!this.isShow)
                return !1;
            this.drawCircle()
        }
    }]),
    i
}() //number 4
, Common = function() {
    function t() {
        this.init()
    }
    return _createClass(t, [{
        key: "init",
        value: function() {
            this.commonFunction()
        }
    }, {
        key: "commonFunction",
        value: function() {
            this.branchContents()
        }
    }, {
        key: "branchContents",
        value: function() {
            var t = this
            , i = document.getElementById("barba-wrapper").querySelector("main.barba-container")
            , e = i.getAttribute("id")
            , s = [];
            switch (this.id = e) {
            }
            for (var n = document.createDocumentFragment(), a = 0; a < s.length; a++) {
                var o = document.createElement("script");
                o.src = s[a],
                n.appendChild(o)
            }
            i.appendChild(n),
            setTimeout(function() {
                t.doPageInit()
            }, 1e3)
        }
    }, {
        key: "doPageInit",
        value: function() {
            "top" === this.id ? this.PAGE = new TopPage : this.PAGE = new ArticlePage,
            this.PAGE.isMobile = this.isMobile
        }
    }]),
    t
}();
window.addEventListener("DOMContentLoaded", function() {
    new Common
});
