"use strict";
var _createClass = function() {
    function s(e, i) {
        for (var t = 0; t < i.length; t++) {
            var s = i[t];
            s.enumerable = s.enumerable || !1,
            s.configurable = !0,
            "value"in s && (s.writable = !0),
            Object.defineProperty(e, s.key, s)
        }
    }
    return function(e, i, t) {
        return i && s(e.prototype, i),
        t && s(e, t),
        e
    }
}();
var TopPage = function() {
    function e() {
        this.init()
    }
    return _createClass(e, [{
        key: "init",
        value: function() {
            var e = this;
            this.initPropery(),
            this.commonEvent(); 
            setTimeout(function() {
                e.introAnim()
            }, 500)
        }
    }, {
        key: "initPropery",
        value: function() {
            var e = this;
            this.SCR = null,
            this.FCIR_01 = new Circle_02({
                id: "js-footer-circle"
            }),
            this.FCIR_02 = new Circle_01({
                id: "js-footer-circle_02"
            }),
            this.FCIR_03 = new Circle_04({
                id: "js-footer-circle_03"
            }),
            this.FCIR_04 = new Circle_03({
                id: "js-footer-circle_04"
            }),
            this.watcherList = [],
            this.isMobile = !1
        }
    }, {
        key: "commonEvent",
        value: function() {
            this.addWatcherInstance()
        }
    }, {
        key: "addWatcherInstance",
        value: function() {
            for (
                var s = this,
                r = document.querySelectorAll(".section--top5"),
                h = 0; h < r.length; h++)
            s.FCIR_01.isShow = s.FCIR_02.isShow = s.FCIR_03.isShow = s.FCIR_04.isShow = !0;
        }
    }, {
        key: "introAnim",
        value: function() {
            this.tick();
        }
    }, {
        key: "tick",
        value: function() {
            var e = this;
            this.FCIR_01.draw(),
            this.FCIR_02.draw(),
            this.FCIR_03.draw(),
            this.FCIR_04.draw(),
            requestAnimationFrame(function() {
                e.tick()
            })
        }
    }]),
    e
}();
