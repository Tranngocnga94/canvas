<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="section__inner">
	<div class="contents-links js-scroll-blc is-scroll-show" id="js-contents-links">
		<h3 class="contents-links__hd hd-number"><span class="txt"><span class="underpage">OTHER </span>TOPICS</span>
		</h3>
		<div class="contents-links__blc contents-links__blc--cnt_01">
			<h4 class="contents-links__subhd">ゼロベ<span class="line">ー</span>スで<br>監査業務を設計・構築</h4>
			<a class="contents-links__item type_01" href="/digital/">
				<p class="txt">デジタルトランス<br>フォーメーション</p>
				<canvas id="js-footer-circle_02" width="150" height="150"></canvas>
			</a>
			<a class="contents-links__item type_02" href="/standardization/">
				<p class="txt">監査業務の標準化</p>
				<canvas id="js-footer-circle" width="150" height="150"></canvas>
			</a>
		</div>
		<div class="contents-links__blc contents-links__blc--cnt_02">
			<h4 class="contents-links__subhd">監査業務の高度化</h4>
			<a class="contents-links__item type_03" href="/technology/">
				<p class="txt">先端的技術の<br>研究・開発</p>
				<canvas id="js-footer-circle_04" width="150" height="150"></canvas>
			</a>
			<a class="contents-links__item type_05" href="/insight/">
				<p class="txt">Big Data × AI</p>
				<canvas id="js-footer-circle_03" width="150" height="150"></canvas>
			</a>
		</div>
	</div>
	<!--.contents-links-->
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>